import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilPage } from '../profil/profil';
import { UserService } from '../services/UserService';

@Component({
  templateUrl: 'build/pages/user/user.html'
})
export class UserPage {
	//buat variable dataUser
	public dataUser;
 	constructor(public navCtrl: NavController,
 		userService:UserService) {
 		//inisialisasi data
 		this.dataUser = userService.getData();
 		// this.dataUser = [
 		// 	{id:1,username:'Adi',alamat:'Cibinong'},
 		// 	{id:2,username:'Ade',alamat:'Ciracas'},
 		// 	{id:3,username:'Adu',alamat:'Cisalak'},
 		// 	{id:4,username:'Ado',alamat:'Cibubur'},
 		// 	{id:5,username:'Ada',alamat:'Citayam'}
 		// ];
  	}
  	tampilProfil(user){
  		console.log("Anda memilih user",user);
  		// cara mengirim data ke controller lain
  		this.navCtrl.push(
  			ProfilPage,//<--componennya
  			{userDipilih:user}//<--data yg dikirim
  						);
  	}

}
