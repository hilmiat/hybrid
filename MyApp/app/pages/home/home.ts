import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// sertakan komponen about
import { AboutPage } from '../about/about';
import { UserPage } from '../user/user';


@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
	//buat variable nama
	public nama;
	public menu;
 	constructor(public navCtrl: NavController) {
 		//inisialisasi var nama
 		this.nama = "Edo";
 		this.menu = 
 			[
 				{tulisan:'About',link:AboutPage},
 				{tulisan:'User',link:UserPage}
 			];
  	}
  
  	pindahHal(dipilih){
  		console.log("Anda menekan tombol");
  		//pindah ke komponen about
  		this.navCtrl.push(dipilih.link);
  	}
  	// pindahHal2(){
  	// 	console.log("Anda menekan tombol");
  	// 	//pindah ke komponen about
  	// 	this.navCtrl.push(UserPage);
  	// }

}
