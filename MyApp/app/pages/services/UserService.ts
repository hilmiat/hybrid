export class UserService{
	public dataUser;
	constructor(){
		this.dataUser = [
 			{id:1,username:'Adi',alamat:'Cibinong'},
 			{id:2,username:'Ade',alamat:'Ciracas'},
 			{id:3,username:'Adu',alamat:'Cisalak'},
 			{id:4,username:'Ado',alamat:'Cibubur'},
 			{id:5,username:'Ada',alamat:'Citayam'}
 		];
	}
	getData(){
		return this.dataUser;
	}
}