import { Component } from '@angular/core';
//jika ingin membaca parameter import NavParams
import { NavController,NavParams } from 'ionic-angular';
@Component({
  templateUrl: 'build/pages/profil/profil.html'
})
export class ProfilPage {
	public user;
  constructor(public navCtrl: NavController,
  	params: NavParams) {
  	//membaca parameter yg dikirim
  	this.user = params.get("userDipilih");
  }
}