Kebutuhan tools:
	+ nodejs & npm
		- download di situs nodejs
		- cek:
			di cmd ketikkan:
			node -v
			npm -v
	+ cordova
		- buka terminal/cmd sebagai administrator
		- ketikkan:
			npm install -g cordova
		- catatan jika menggunakan proxy:
			set http_proxy=......
			set https_proxy=......
			npm config set proxy ...
			npm config set https_proxy ...
		- cek apakah sudah terinstall
			cordova -v
	+ ionic:cara install sama seperti cordova
		npm install -g ionic
		ionic -v
=======================
# create ionic project:
	+ pindah ke lokasi/folder tempat project akan dibuat
	+ ionic start nama_folder template versi
		ionic start MyApp blank --v2
# menjalankan project
	+ pindah ke direktori project
	+ jalankan:
		ionic serve -l
	+ buka browser
# membuat aplikasi android (.apk)
	+ tambahkan platform android:
		ionic platform add android
	+ build apk
		ionic build android
		- catatan: butuh android sdk
			+ install android sdk (butuh jdk)
			+ install API yang dibutuhkan:
				- Android SDK Tools
				- Android SDK Platform-tools
				- Android SDK build-tools(api 23)
				- SDK Platform (api 23)
			+ catatan: jika menggunakan proxy, set proxy pada android sdk manager
				- tools > options
			+ set environtment variable:
				- ANDROID_HOME:path_to_sdk
				- PATH tambah dgn ANDROID_HOME\tools
				- cara cek:
				ketikkan pada cmd:
					android 


