import { Injectable } from '@angular/core';
//1. tambahkan SqlStorage dan Storage
import { SqlStorage,Storage } from 'ionic-angular';
@Injectable()
export class UserService {
	//2. buat variable storage dan data
	private storage: Storage;
  constructor() {
  	//3. buat objek storage
  	this.storage = new Storage(SqlStorage);
  	//4. definisikan tabel
  	this.storage.query("CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, firstname TEXT, lastname TEXT)");
  }

  getAll(){
  	return this.storage.query("SELECT * FROM user ORDER BY id DESC");
  }

  addUser(dataInsert){
  	return this.storage.query("INSERT INTO user (firstname,lastname) values(?,?)",
			[dataInsert.firstname,dataInsert.lastname]);
  }
 	
  delete(iddelete){
  	return this.storage.query("DELETE FROM user WHERE id=?",[iddelete]);
  }
  update(userupdate){
  	return this.storage.query("UPDATE user SET firstname=?,lastname=? WHERE id=?",
  		[userupdate.firstname,userupdate.lastname,userupdate.id]);
  }
}

