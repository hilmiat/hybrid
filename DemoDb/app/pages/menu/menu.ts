import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { UserwsPage } from '../userws/userws';

@Component({
  templateUrl: 'build/pages/menu/menu.html',
})
export class MenuPage {

  constructor(private navCtrl: NavController) {
  }
  pindahKeHome(){
  	this.navCtrl.push(HomePage);
  }
  pindahKeUserws(){
  	this.navCtrl.push(UserwsPage);
  }

}
