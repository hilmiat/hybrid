import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { UserService } from '../../providers/user-service/user-service';
@Component({
  templateUrl: 'build/pages/add-user/add-user.html',
  providers:[UserService]
})
export class AddUserPage {
	public user;
  constructor(private navCtrl: NavController,public us :UserService, param:NavParams) {
  		let userupdate = param.get("data"); 
  		if(userupdate != undefined){
  			this.user = userupdate;
  		}else{
  			this.user = {firstname:'',lastname:'',id:''};
  		}
  }
  public simpan(){
  	console.log("data disimpan:",this.user);
  	if(this.user.id > 0){
  		//update
  		this.us.update(this.user)
	  		.then(
	  			(data)=>{
	  				//jika sukses, kembali ke home
	  				this.navCtrl.pop();
	  			},
	  			(error)=>{
	  				//jika error tampilkan error
	  				console.log("terjadi Kesalahan ",error);
	  			}
	  			);
  	}else{
  		//simpan
	  	this.us.addUser(this.user)
	  		.then(
	  			(data)=>{
	  				//jika sukses, kembali ke home
	  				this.navCtrl.pop();
	  			},
	  			(error)=>{
	  				//jika error tampilkan error
	  				console.log("terjadi Kesalahan ",error);
	  			}
	  			);
	}
  }
}
