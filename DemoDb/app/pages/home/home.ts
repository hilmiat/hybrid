import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { AddUserPage } from '../add-user/add-user';
import { UserService } from '../../providers/user-service/user-service';


@Component({
  templateUrl: 'build/pages/home/home.html',
  providers:[UserService]
})
export class HomePage {
	public data: Array<Object>;
  
  constructor(public navCtrl: NavController,public userService: UserService) {
  	//inisialisasi data
  	this.data = [];
  }

  // baca data
  public bacaData(){
  	 this.userService.getAll()
  		.then(
  			(data)=>{
  				//jika suskes query, dan ada datanya
  				if(data.res.rows.length > 0){
  					//simpan data ke array
  					this.data = [];
  					for(let i=0;i<data.res.rows.length; i++){
  						this.data.push(
  							{
  								"id":data.res.rows.item(i).id,
  								"firstname":data.res.rows.item(i).firstname,
  								"lastname":data.res.rows.item(i).lastname
  							}
  						);
  					}
  				}
  			},
  			(error)=>{
  				//jika proses query error
  				console.log("Ada Kesalahan",error);
  			}
  		);
  }
	
	//7. add data
	public tambahData(){
    //pindah ke halaman add
    this.navCtrl.push(AddUserPage);
	}

  public removeItem(dataRemove){
    this.userService.delete(dataRemove.id)
      .then(
        (data)=>{this.bacaData()},(error)=>{console.log("ada eror",error)}
      );
  }

  public update(userupdate){
    this.navCtrl.push(AddUserPage,{'data':userupdate});
  }
	//8. ketika page ditampilkan, panggil method baca data
	// public onPageLoaded(){ //hanya akan dipanggil 1 kali saja
	// 	this.bacaData();
	// }
  
  // akan dieksekusi setiap kali page aktif
  public onPageDidEnter(){
    this.bacaData();
  }
}
