import { Component } from '@angular/core';
import { NavController, LoadingController,
	 ModalController } from 'ionic-angular';
import { ProfilPage } from '../profil/profil';
//langkah 1: import provider
import { DataWs } from '../../providers/data-ws/data-ws';


@Component({
  templateUrl: 'build/pages/userws/userws.html',
  //langkah 2 daftarkan provider
  providers:[DataWs]
})
export class UserwsPage {
	//langkah 3: buat variable
	public data_user: Array<Object>;
	//langkah 4: mapping pada argumen konstruktor
  constructor(private navCtrl: NavController,
  		public loading:LoadingController,
  		public modal:ModalController,
  		private ds:DataWs ) {
  }
  	//langkah 5: baca data dari ws
  	bacaData(){
  		let loader = this.loading.create({
  			content:"Please wait..."
  			//duration:3000
  		});
  		loader.present();

  		this.ds.getAllUser().subscribe(
  				data => {
  					this.data_user = data;
  					loader.dismiss();
  				},err => {
  					loader.dismiss();
  					console.log("error baca data:",err);
  				}
  			);
  	}
  	//langkah 6: BACA data setiap kali page ditampilkan
  	onPageDidEnter(){
  		this.bacaData();
  	}
  	detail(user){
  		this.ds.getUserById(user.id)
  			.subscribe(data=>{
  				console.log(data);
  				let modal_loader = 
  					this.modal.create(ProfilPage,{"data":user});
  				modal_loader.present();
  			});
  	}
}
