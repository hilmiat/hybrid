import { Component } from '@angular/core';
import { NavController , NavParams, ViewController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/profil/profil.html',
})
export class ProfilPage {
	public user;
  constructor(private navCtrl: NavController,
  	param:NavParams,private viewCtrl:ViewController) {
  	var data = param.get("data");
  	if(data != undefined){
  		this.user = data;
  	}
  }
  closeModal(){
  	this.viewCtrl.dismiss();
  }

}
